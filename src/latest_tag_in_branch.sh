#!/usr/bin/env bash

SIMPLE_SEMVER_REGEX="\([0-9]*\)[.]\([0-9]*\)[.]\([0-9]*\)"

function showUsage() {
  echo "usage: latest_tag_in_branch.sh [help] [<branch>]"
  echo "       help:     Shows this help message."
  echo "       <branch>: Select the branch to check, default is the current branch."
  echo ""
  echo "Selects the tag with the greatest version number in the <branch>."
  echo "Only release tags with major.minor.patch are selected. Any tag "
  echo "with a pre release version is ignored."
}

if [[ "$#" -eq 1 ]]; then
  if [[ "$1" != "help" ]]; then
    git tag --merged "$1" | grep -e "^${SIMPLE_SEMVER_REGEX}$" | tail -1
    exit 0;
  fi
  showUsage
  exit 0;
fi
if [[ "$#" -gt 1 ]]; then
  showUsage
  exit 0;
fi
git tag --merged | grep -e "^${SIMPLE_SEMVER_REGEX}$" | tail -1
