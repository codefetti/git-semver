#!/usr/bin/env bash

function showUsage() {
  echo "usage: changelog.sh [help|extract|release]"
  echo "                    [-f <file>]"
  echo "                    [-h <hl_prefix>]"
  echo "                    [-d <date>]"
  echo "                    [-v <version>]"
  echo "                    [-l <last_version>]"
  echo "                    [-p <project_base_url>]"
  echo "                    [-m <unreleased_message>]"
  echo "       help: Shows this help message."
  echo "       extract: Extracts the Unreleased section or the section of <version>"
  echo "           if -v is given of the changelog file and prints it to standard"
  echo "           out."
  echo "       release: Updates the unreleased section to the new release"
  echo "           version and inserts a new unreleased section."
  echo "       <file>: Location of the changelog markdown file,"
  echo "           defaults to 'CHANGELOG.md'."
  echo "       <hl_prefix>: The prefix of headlines that mark releases,"
  echo "           defaults to '## '"
  echo "       <date>: The date to be used for the new version headline,"
  echo "           defaults to today in the format 'YYYY-MM-DD'"
  echo "       <version>: The version which changes should be extracted or"
  echo "           the version to release."
  echo "       <last_version>: The version of the last release is used to link to"
  echo "           proper commit lists"
  echo "       <project_base_url>: The base url of the project used to generate"
  echo "           links in the changelog, e.g. the 'CI_PROJECT_URL' in GitLab CI."
  echo "       <unreleased_message>: The The message that is added to a new"
  echo "           unreleased section. The default is: 'No notable changes yet.'"
}


function parseArgs() {
  FILE="CHANGELOG.md"
  RELEASE_HEADLINE_SEARCH="## "
  DATE=`date "+%Y-%m-%d"`
  VERSION=""
  LAST_VERSION=""
  PROJECT_BASE_URL=""
  UNRELEASED_MESSAGE="No notable changes yet."
  for p in "$@"; do
    case "$p" in
      -f)
      shift # past argument
      FILE="$1"
      ;;
      -h)
      shift # past argument
      RELEASE_HEADLINE_SEARCH="$1"
      ;;
      -d)
      shift # past argument
      DATE="$1"
      ;;
      -v)
      shift # past argument
      VERSION="$1"
      ;;
      -p)
      shift # past argument
      PROJECT_BASE_URL="$1"
      ;;
      -l)
      shift # past argument
      LAST_VERSION="$1"
      ;;
      -m)
      shift # past argument
      UNRELEASED_MESSAGE="$1"
      ;;
      *)
      shift
      ;;
    esac
  done
  if [[ ! -f "$FILE" ]]; then
    echo "File '${FILE}' does not exist." >&2
    showUsage
    exit 1
  fi
}

function release() {
  file="$1"
  version="$2"
  baseUrl="$3"
  date="$4"
  oldVersion="$5"
  headlineSearch="$6"
  unreleasedMessage="$7"
  search="Unreleased"
  linkPath="/commits/${version}"
  if [[ -z "$baseUrl" ]]; then
    echo "-p is required to create a release" >&2
    showUsage
    exit 1
  fi
  if [[ -z "$version" ]]; then
    echo "-v is required to create a release" >&2
    showUsage
    exit 1
  fi
  if [[ -n "$oldVersion" ]]; then
    linkPath="/compare/${oldVersion}...${version}"
  fi
  while IFS= read -r line
  do
    search_hl=`echo "$line" | grep -e "^${headlineSearch}.*${search}.*$"`
    if [[ -n "$search_hl" ]]; then
      echo "$line"
      echo ""
      echo "${unreleasedMessage}"
      echo ""
      echo ""
      echo "${headlineSearch}[${version}](${baseUrl}${linkPath}) - ${date}"
    else
      echo "$line"
    fi
  done < "$file"
}

function extract() {
  file="$1"
  search="Unreleased"
  version="$2"
  headlineSearch="$3"
  if [[ -n "$version" ]]; then
    v=`echo "$version" | sed 's/\./\\\\./g'`
    search="\[$v]"
  fi
  in_search="0"
  while IFS= read -r line
  do
    hl=`echo "$line" | grep -e "^${headlineSearch}.*$"`
    search_hl=`echo "$line" | grep -e "^${headlineSearch}.*${search}.*$"`
    if [[ -n "$search_hl" ]]; then
      in_search="1"
    elif [[ -n "$hl" ]]; then
      if [[ "1" = "$in_search" ]]; then
        break
      fi
    fi
    if [[ "1" = "$in_search" ]]; then
      echo "$line"
    fi
  done < "$file"
}

if [[ "help" = "$1" ]]; then
  showUsage
  exit 0;
elif [[ "extract" = "$1" ]]; then
  parseArgs "$@"
  extract "$FILE" "$VERSION" "$RELEASE_HEADLINE_SEARCH"
  exit $?;
elif [[ "release" = "$1" ]]; then
  parseArgs "$@"
  release "$FILE" "$VERSION" "$PROJECT_BASE_URL" "$DATE" "$LAST_VERSION" "$RELEASE_HEADLINE_SEARCH" "$UNRELEASED_MESSAGE"
  exit $?;
else
  showUsage
  exit 0;
fi
