#!/usr/bin/env bash

function showUsage() {
  echo "usage: bump_yaml.sh [help|get|set] [-b <bump>] [-f <file>]"
  echo "       help:   Shows this help message."
  echo "       get:    Extracts the 'bump' property from the given file."
  echo "       set:    Updates the 'bump' property in the given file."
  echo "       <file>: Location of the changelog markdown file or the yaml"
  echo "               file containing the bump information in the yaml"
  echo "               header of the markdown file or in the yaml file,"
  echo "               defaults to 'CHANGELOG.md'."
  echo "       <bump>: One of [major|minor|patch] to set as new 'bump'"
  echo "               where 'patch' is the default."
  echo ""
  echo "Reads and updates the 'bump' property from a yaml file."
}

function parseArgs() {
  FILE="CHANGELOG.md"
  BUMP="patch"
  for p in "$@"; do
    case "$p" in
      -f)
      shift # past argument
      FILE="$1"
      ;;
      -b)
      shift # past argument
      BUMP="$1"
      ;;
      *)
      shift
      ;;
    esac
  done
}

function isKey() {
  local key="$1"
  local line="$2"
  echo "$line" | grep -e "^$key: *.*$"
}
function stringValue() {
  local key="$1"
  local line="$2"
  local value=`echo "$line" | sed -e "s/^${key}: *\(.*\)$/\\1/"`
  local value=`echo "$value" | sed -e 's/^"\(.*\)" *$/\\1/'`
  local value=`echo "$value" | sed -e "s/^'\(.*\)' *$/\\1/"`
  echo "$value"
}

function echoBumpAndExit() {
  local bump="$1"
  case "$bump" in
    patch)
      echo "patch"
      exit 0
      ;;
    minor)
      echo "minor"
      exit 0
      ;;
    major)
      echo "major"
      exit 0
      ;;
    *)
      echo "bump must be major minor or patch but is '$bump'" >&2
      exit 1
  esac
}

function getBumpFromMd() {
  file="$1"
  inFile=''
  inYamlHeader=''
  while IFS= read -r line
  do
    if [[ -z "$inFile" ]]; then
      if [[ "---" == "$line" ]]; then
        inYamlHeader="1"
      else
        echo "No Yaml header in markdown" >&2
        exit 1
      fi
      inFile="1"
    else
      if [[ "---" == "$line" ]]; then
        inYamlHeader=""
        echo "out"
        exit 0;
      fi
    fi
    if [[ -n "$inYamlHeader" ]]; then
      isBump=`isKey "bump" "$line"`
      if [[ -n "$isBump" ]]; then
        bump=`stringValue "bump" "$line"`
        echoBumpAndExit "$bump"
      fi
    fi
  done < "$file"
}

function getBumpFromYaml() {
  file="$1"
  while IFS= read -r line
  do
    isBump=`isKey "bump" "$line"`
    if [[ -n "$isBump" ]]; then
      bump=`stringValue "bump" "$line"`
      echoBumpAndExit "$bump"
    fi
  done < "$file"
}


function getBump() {
  file="$1"
  if [[ "${file: -3}" == ".md" ]]; then
    getBumpFromMd "$file"
  else
    getBumpFromYaml "$file"
  fi
}

function setBumpInMd() {
  file="$1"
  bump="$2"
  inFile=''
  inYamlHeader=''
  bumpUpdated="false"
  while IFS= read -r line
  do
    if [[ -z "$inFile" ]]; then
      if [[ "---" == "$line" ]]; then
        inYamlHeader="1"
      else
        echo "---"
        echo "bump: $bump"
        echo "---"
        bumpUpdated="true"
      fi
      inFile="1"
    else
      if [[ -n "$inYamlHeader" ]]; then
        if [[ "---" == "$line" ]]; then
          if [[ "$bumpUpdated" == "false" ]]; then
            echo "bump: $bump"
          fi
          inYamlHeader=""
        fi
      fi
    fi
    if [[ -n "$inYamlHeader" ]]; then
      isBump=`isKey "bump" "$line"`
      if [[ -n "$isBump" ]]; then
        echo "bump: $bump"
        bumpUpdated="true"
      else
        echo "$line"
      fi
    else
      echo "$line"
    fi
  done < "$file"
}

function setBumpInYaml() {
  file="$1"
  bump="$2"
  bumpUpdated="false"
  while IFS= read -r line
  do
    isBump=`isKey "bump" "$line"`
    if [[ -n "$isBump" ]]; then
      echo "bump: $bump"
      bumpUpdated="true"
    else
      echo "$line"
    fi
  done < "$file"
  if [[ "$bumpUpdated" == "false" ]]; then
    echo "bump: $bump"
  fi
}

function setBump() {
  file="$1"
  bump="$2"
  if [[ "${file: -3}" == ".md" ]]; then
    setBumpInMd "$file" "$bump"
  else
    setBumpInYaml "$file" "$bump"
  fi
}

if [[ "help" = "$1" ]]; then
  showUsage
  exit 0;
elif [[ "get" = "$1" ]]; then
  parseArgs "$@"
  getBump "$FILE"
  exit 0;
elif [[ "set" = "$1" ]]; then
  parseArgs "$@"
  setBump "$FILE" "$BUMP"
  exit 0;
else
  showUsage
  exit 0;
fi

