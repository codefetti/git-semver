#!/usr/bin/env bash

SIMPLE_SEMVER_REGEX="\([0-9]*\)[.]\([0-9]*\)[.]\([0-9]*\)"

function showUsage() {
  echo "usage: semver.sh [help|major|minor|patch|bump] -v <version> [-b <bump>]"
  echo "       help:      Shows this help message."
  echo "       major:     Extracts the major version of <version>."
  echo "       minor:     Extracts the minor version of <version>."
  echo "       patch:     Extracts the patch version of <version>."
  echo "       bump:      Increments the <bump> part of <version> according to"
  echo "                  the rules of semantic versioning."
  echo "       <version>: A semantic version without pre release."
  echo "       <bump>:    One of [major|minor|patch] the desired version bump"
  echo "                  where 'patch' is the default."
  echo ""
  echo "Utility to deal with semantic versions."
}

function parseArgs() {
  VERSION=""
  BUMP="patch"
  for p in "$@"; do
    case "$p" in
      -v)
      shift # past argument
      VERSION="$1"
      ;;
      -b)
      shift # past argument
      BUMP="$1"
      ;;
      *)
      shift
      ;;
    esac
  done
  if [[ -z "$VERSION" ]]; then
    echo "Version is missing" >&2
    echo ""
    showUsage
    exit 1
  fi
  local SV=`echo "$VERSION" | sed -e "s!${SIMPLE_SEMVER_REGEX}!!"`
  if [[ -n "$SV" ]]; then
    echo "Version '${VERSION}' is invalid" >&2
    echo ""
    showUsage
    exit 1
  fi
}

function major() {
  MAJOR=`echo "$1" | sed -e "s!${SIMPLE_SEMVER_REGEX}!\1!"`
  echo "$MAJOR"
}

function minor() {
  MINOR=`echo "$1" | sed -e "s!${SIMPLE_SEMVER_REGEX}!\2!"`
  echo "$MINOR"
}

function patch() {
  PATCH=`echo "$1" | sed -e "s!${SIMPLE_SEMVER_REGEX}!\3!"`
  echo "$PATCH"
}

function bump() {
  local MAJOR=`major "$1"`
  local MINOR=`minor "$1"`
  local PATCH=`patch "$1"`
  case "$2" in
    major)
    MAJOR=$((MAJOR+1))
    MINOR="0"
    PATCH="0"
    ;;
    minor)
    MAJOR="$MAJOR"
    MINOR=$((MINOR+1))
    PATCH="0"
    ;;
    patch)
    MAJOR="$MAJOR"
    MINOR="$MINOR"
    PATCH=$((PATCH+1))
    ;;
    *)
    echo "Bump must be one of major, minor or patch but was '$BUMP'" >&2
    echo ""
    showUsage
    exit 1
    ;;
  esac
  echo "${MAJOR}.${MINOR}.${PATCH}"
}

if [[ "help" = "$1" ]]; then
  showUsage
  exit 0;
elif [[ "major" = "$1" ]]; then
  parseArgs "$@"
  major "$VERSION"
  exit 0;
elif [[ "minor" = "$1" ]]; then
  parseArgs "$@"
  minor "$VERSION"
  exit 0;
elif [[ "patch" = "$1" ]]; then
  parseArgs "$@"
  patch "$VERSION"
  exit 0;
elif [[ "bump" = "$1" ]]; then
  parseArgs "$@"
  bump "$VERSION" "$BUMP"
  exit $?;
else
  showUsage
  exit 0;
fi
