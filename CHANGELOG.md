---
# This Yaml header defines the version bump as major, minor or patch.
bump: patch
---
# Changelog of git-semver

All notable changes to git-semver will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## Unreleased

No notable changes yet.


## [0.1.2](https://gitlab.com/codefetti/git-semver/compare/0.1.1...0.1.2) - 2019-10-06

### Fixed

- Docker tags for minor and major version are now correctly created.


## [0.1.1](https://gitlab.com/codefetti/git-semver/compare/0.1.0...0.1.1) - 2019-10-06

### Added

- Utility to extract the latest Git tag of a branch  
  `latest_tag_in_branch.sh` can be executed on a Git repository and derives the latest tag with a
   semantic version _(major.minor.patch)_ that is included in the current branch.
- Utility to read and increment version parts according to 
  [semantic versioning](https://semver.org/)  
  `semver.sh` can extract major, minor and patch version parts of a semantic versioning string and
  is able to bump one of them.
- Utility to extract parts from a changelog and update a changelog when releasing  
  `changelog.md` is able to parse [a changelog](https://keepachangelog.com). It can extract the 
  release notes of documented versions and update the "Unreleased" with the next version headline
   and insert a new unreleased section.
- Utility to support definition of the next version bump in Yaml or a Markdown Yaml header.  
  `bump_yaml.sh` can read the property `bump` as _major_, _minor_ or _patch_ from a Yaml file or 
  a Yaml header embedded in a Markdown file. It can also set the property which can be used for a
  reset after a new release which was not bumped with the default.

Note: All utilities dealing with semantic versioning can not handle pre release and build meta 
data yet. Only _major_, _minor_ and _patch_ are supported.
