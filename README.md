# Git-Semver

An Alpine container with bash, git, openssh, jq, curl and some shell scripts to support computation 
around semantic version. All notable changes are documented in the [CHANGELOG.md](./CHANGELOG.md)

## Bash Scripts in the path

### `latest_tag_in_branch.sh`

```
usage: latest_tag_in_branch.sh [help] [<branch>]
       help:     Shows this help message.
       <branch>: Select the branch to check, default is the current branch.

Selects the tag with the greatest version number in the <branch>.
Only release tags with major.minor.patch are selected. Any tag 
with a pre release version is ignored.
```

### `semver.sh`

```
usage: semver.sh [help|major|minor|patch|bump] -v <version> [-b <bump>]
       help:      Shows this help message.
       major:     Extracts the major version of <version>.
       minor:     Extracts the minor version of <version>.
       patch:     Extracts the patch version of <version>.
       bump:      Increments the <bump> part of <version> according to
                  the rules of semantic versioning.
       <version>: A semantic version without pre release.
       <bump>:    One of [major|minor|patch] the desired version bump
                  where 'patch' is the default.

Utility to deal with semantic versions.
```

### `changelog.sh`

```
usage: changelog.sh [help|extract|release]
                    [-f <file>]
                    [-h <hl_prefix>]
                    [-d <date>]
                    [-v <version>]
                    [-l <last_version>]
                    [-p <project_base_url>]
                    [-m <unreleased_message>]
       help: Shows this help message.
       extract: Extracts the Unreleased section or the section of <version>
           if -v is given of the changelog file and prints it to standard
           out.
       release: Updates the unreleased section to the new release
           version and inserts a new unreleased section.
       <file>: Location of the changelog markdown file,
           defaults to 'CHANGELOG.md'.
       <hl_prefix>: The prefix of headlines that mark releases,
           defaults to '## '
       <date>: The date to be used for the new version headline,
           defaults to today in the format 'YYYY-MM-DD'
       <version>: The version which changes should be extracted or
           the version to release.
       <last_version>: The version of the last release is used to link to
           proper commit lists
       <project_base_url>: The base url of the project used to generate
           links in the changelog, e.g. the 'CI_PROJECT_URL' in GitLab CI.
       <unreleased_message>: The The message that is added to a new
           unreleased section. The default is: 'No notable changes yet.'
```

### `bump_yaml.sh`

```
usage: bump_yaml.sh [help|get|set] [-b <bump>] [-f <file>]
       help:   Shows this help message.
       get:    Extracts the 'bump' property from the given file.
       set:    Updates the 'bump' property in the given file.
       <file>: Location of the changelog markdown file or the yaml
               file containing the bump information in the yaml
               header of the markdown file or in the yaml file,
               defaults to 'CHANGELOG.md'.
       <bump>: One of [major|minor|patch] to set as new 'bump'
               where 'patch' is the default.

Reads and updates the 'bump' property from a yaml file.
```


## Testing

Run unit tests from the console locally:

```
you@machine:~/projects/git-semver$ test/all_tests.sh
```
