#!/usr/bin/env bash

FAIL="0"
PASSED='\033[0;32mpassed\033[0m'
FAILED='\033[0;31mfailed\033[0m'

function execTest() {
  actual=`/bin/bash -c "$1"`
  if [[ "$actual" = "$2" ]]; then
    echo -e "${PASSED}: $1 => $2"
  else
    echo -e "${FAILED}: $1" >&2
    echo "        expected '$actual' to be '$2'"
    FAIL="1"
  fi
}

PATH_TO_SRC=""
if [[ -n "$1" ]]; then
  PATH_TO_SRC="$1"
fi

execTest "${PATH_TO_SRC}semver.sh major -v 1.2.3" "1"
execTest "${PATH_TO_SRC}semver.sh major -v 2.20.30" "2"
execTest "${PATH_TO_SRC}semver.sh major -v 11.22.33" "11"

execTest "${PATH_TO_SRC}semver.sh minor -v 1.2.3" "2"
execTest "${PATH_TO_SRC}semver.sh minor -v 2.20.30" "20"
execTest "${PATH_TO_SRC}semver.sh minor -v 11.22.33" "22"

execTest "${PATH_TO_SRC}semver.sh patch -v 1.2.3" "3"
execTest "${PATH_TO_SRC}semver.sh patch -v 2.20.30" "30"
execTest "${PATH_TO_SRC}semver.sh patch -v 11.22.33" "33"

execTest "${PATH_TO_SRC}semver.sh bump -v 1.2.3 -b major" "2.0.0"
execTest "${PATH_TO_SRC}semver.sh bump -v 1.2.3 -b minor" "1.3.0"
execTest "${PATH_TO_SRC}semver.sh bump -v 1.2.3 -b patch" "1.2.4"
execTest "${PATH_TO_SRC}semver.sh bump -v 1.2.3" "1.2.4"

execTest "${PATH_TO_SRC}semver.sh bump -v 0.0.0 -b major" "1.0.0"
execTest "${PATH_TO_SRC}semver.sh bump -v 0.0.0 -b minor" "0.1.0"
execTest "${PATH_TO_SRC}semver.sh bump -v 0.0.0 -b patch" "0.0.1"
execTest "${PATH_TO_SRC}semver.sh bump -v 0.0.0" "0.0.1"

if [[ "1" = "$FAIL" ]]; then
  exit 1
fi
