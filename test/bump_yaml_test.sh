#!/usr/bin/env bash

PATH_TO_TESTS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

FAIL="0"
PASSED='\033[0;32mpassed\033[0m'
FAILED='\033[0;31mfailed\033[0m'

function execTest() {
  if [[ "ERROR" != "$2" ]]; then
    actual=`/bin/bash -c "$1"`
    if [[ "$actual" = "$2" ]]; then
      if (( $(grep -c . <<<"$2") > 1 )); then
      echo -e "${PASSED}: $1 => <too much content>"
      else
        echo -e "${PASSED}: $1 => $2"
      fi
    else
      echo -e "${FAILED}: $1" >&2
      echo "        expected '$actual' to be '$2'"
      FAIL="1"
    fi
  else
    actual=`/bin/bash -c "$1"`
    if [[ "0" != "$?" ]]; then
      echo -e "${PASSED}: $1 => ERROR"
    else
      echo -e "${FAILED}: $1" >&2
      echo "        expected to result in ERROR, but succeeded"
    fi
  fi
}

PATH_TO_SRC=""
if [[ -n "$1" ]]; then
  PATH_TO_SRC="$1"
fi

execTest "${PATH_TO_SRC}bump_yaml.sh get -f '${PATH_TO_TESTS}/test-data/initial.md'" "minor"
execTest "${PATH_TO_SRC}bump_yaml.sh get -f '${PATH_TO_TESTS}/test-data/second.md'" "patch"
execTest "${PATH_TO_SRC}bump_yaml.sh get -f '${PATH_TO_TESTS}/test-data/bump_single_quoted.md'" "major"
execTest "${PATH_TO_SRC}bump_yaml.sh get -f '${PATH_TO_TESTS}/test-data/bump_double_quoted.md'" "major"
execTest "${PATH_TO_SRC}bump_yaml.sh get -f '${PATH_TO_TESTS}/test-data/bump_single_quoted.yaml'" "major"
execTest "${PATH_TO_SRC}bump_yaml.sh get -f '${PATH_TO_TESTS}/test-data/bump_double_quoted.yaml'" "major"

expected=`cat "${PATH_TO_TESTS}/test-data/no_bump_property.expected.yaml"`
execTest "${PATH_TO_SRC}bump_yaml.sh set -f '${PATH_TO_TESTS}/test-data/no_bump_property.yaml' -b minor" "$expected"
expected=`cat "${PATH_TO_TESTS}/test-data/bump_property.expected.yaml"`
execTest "${PATH_TO_SRC}bump_yaml.sh set -f '${PATH_TO_TESTS}/test-data/bump_property.yaml' -b patch" "$expected"

expected=`cat "${PATH_TO_TESTS}/test-data/no_bump_property.expected.md"`
execTest "${PATH_TO_SRC}bump_yaml.sh set -f '${PATH_TO_TESTS}/test-data/no_bump_property.md' -b minor" "$expected"
expected=`cat "${PATH_TO_TESTS}/test-data/bump_property.expected.md"`
execTest "${PATH_TO_SRC}bump_yaml.sh set -f '${PATH_TO_TESTS}/test-data/bump_property.md' -b patch" "$expected"
expected=`cat "${PATH_TO_TESTS}/test-data/initial_hl3_set_bump.expected.md"`
execTest "${PATH_TO_SRC}bump_yaml.sh set -f '${PATH_TO_TESTS}/test-data/initial_hl3.md' -b major" "$expected"

if [[ "1" = "$FAIL" ]]; then
  exit 1
fi
