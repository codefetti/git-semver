---
# This Yaml header defines the version bump as major, minor or patch.
bump: minor
---
# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## Unreleased

No notable changes yet.


## [1.0.0](https://.../commits/1.0.0) - 2019-05-08

### Added
- Some new feature
