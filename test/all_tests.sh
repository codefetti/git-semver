#!/usr/bin/env bash

PATH_TO_TESTS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

SRC=`which semver.sh`
PATH_TO_SRC="$PATH_TO_TESTS/../src/"
if [[ -n "$SRC" ]]; then
  PATH_TO_SRC=""
elif [[ -n "$1" ]]; then
  PATH_TO_SRC="$1"
fi

FAIL="0"

function execTests() {
  echo -e "\033[1;36mRunning $1\033[0m"
  bash -c "$PATH_TO_TESTS/$1 $PATH_TO_SRC"
  if [[ "$?" = "1" ]]; then
    echo -e "\033[1;31mFinished with errors: $1\033[0m"
    FAIL="1"
  else
    echo -e "\033[1;32mFinished successfully: $1\033[0m"
  fi
}

execTests "semver_test.sh"
execTests "changelog_test.sh"
execTests "bump_yaml_test.sh"
execTests "integration_test.sh"

if [[ "1" = "$FAIL" ]]; then
  exit 1
fi
