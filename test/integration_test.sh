#!/usr/bin/env bash

FAIL="0"
PASSED='\033[0;32mpassed\033[0m'
FAILED='\033[0;31mfailed\033[0m'

function execTest() {
  actual=`/bin/bash -c "$1"`
  if [[ "$actual" = "$2" ]]; then
    echo -e "${PASSED}: $1 => $2"
  else
    echo -e "${FAILED}: $1" >&2
    echo "        expected '$actual' to be '$2'"
    FAIL="1"
  fi
}

PATH_TO_SRC=""
if [[ -n "$1" ]]; then
  PATH_TO_SRC="$1"
fi

# prepare

PWD_ORG=`pwd`
TMP=`mktemp -d`
cd "$TMP"

git config --global user.email "a.test.user@codefetti.org"
git config --global user.name "Test User"

git init .
echo "Only for testing" > a_test_README.md
git add a_test_README.md
git commit -m "Initial test commit"
git tag -a "1002.1003.1004" -m "Just a test"
git tag -a "1002.1003.1" -m "Just a test to ignore"

execTest "${PATH_TO_SRC}semver.sh bump -v `${PATH_TO_SRC}latest_tag_in_branch.sh` -b major" "1003.0.0"
execTest "${PATH_TO_SRC}semver.sh bump -v `${PATH_TO_SRC}latest_tag_in_branch.sh` -b minor" "1002.1004.0"
execTest "${PATH_TO_SRC}semver.sh bump -v `${PATH_TO_SRC}latest_tag_in_branch.sh` -b patch" "1002.1003.1005"

cd "$PWD_ORG"
if [[ "1" = "$FAIL" ]]; then
  exit 1
fi
