#!/usr/bin/env bash

PATH_TO_TESTS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

FAIL="0"
PASSED='\033[0;32mpassed\033[0m'
FAILED='\033[0;31mfailed\033[0m'

function execTest() {
  if [[ "ERROR" != "$2" ]]; then
    actual=`/bin/bash -c "$1"`
    if [[ "$actual" = "$2" ]]; then
      if (( $(grep -c . <<<"$2") > 1 )); then
      echo -e "${PASSED}: $1 => <too much content>"
      else
        echo -e "${PASSED}: $1 => $2"
      fi
    else
      echo -e "${FAILED}: $1" >&2
      echo "        expected '$actual' to be '$2'"
      FAIL="1"
    fi
  else
    actual=`/bin/bash -c "$1"`
    if [[ "0" != "$?" ]]; then
      echo -e "${PASSED}: $1 => ERROR"
    else
      echo -e "${FAILED}: $1" >&2
      echo "        expected to result in ERROR, but succeeded"
    fi
  fi
}

PATH_TO_SRC=""
if [[ -n "$1" ]]; then
  PATH_TO_SRC="$1"
fi

execTest "${PATH_TO_SRC}changelog.sh extract -f '${PATH_TO_TESTS}/test-data/does_not_exist'" "ERROR"

expected=`cat "${PATH_TO_TESTS}/test-data/initial_extract.expected.md"`
execTest "${PATH_TO_SRC}changelog.sh extract -f '${PATH_TO_TESTS}/test-data/initial.md'" "$expected"

expected=`cat "${PATH_TO_TESTS}/test-data/initial_hl3_extract.expected.md"`
execTest "${PATH_TO_SRC}changelog.sh extract -f '${PATH_TO_TESTS}/test-data/initial_hl3.md' -h '### '" "$expected"

expected=`cat "${PATH_TO_TESTS}/test-data/second_extract.expected.md"`
execTest "${PATH_TO_SRC}changelog.sh extract -f '${PATH_TO_TESTS}/test-data/second.md'" "$expected"

expected=`cat "${PATH_TO_TESTS}/test-data/second_extract.1.0.0.expected.md"`
execTest "${PATH_TO_SRC}changelog.sh extract -f '${PATH_TO_TESTS}/test-data/second.md' -v 1.0.0" "$expected"

expected=`cat "${PATH_TO_TESTS}/test-data/initial_1.0.0.expected.md"`
execTest "${PATH_TO_SRC}changelog.sh release -f '${PATH_TO_TESTS}/test-data/initial.md' -v 1.0.0 -p https://... -d 2019-05-08" "$expected"

expected=`cat "${PATH_TO_TESTS}/test-data/second_release.1.0.1.expected.md"`
execTest "${PATH_TO_SRC}changelog.sh release -f '${PATH_TO_TESTS}/test-data/second.md' -v 1.0.1 -l 1.0.0 -p https://... -d 2019-05-10" "$expected"


if [[ "1" = "$FAIL" ]]; then
  exit 1
fi
