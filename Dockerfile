FROM alpine:3.9

MAINTAINER Jörg Siebahn <joerg@codefetti.org>

RUN apk update && apk upgrade && apk add --no-cache bash git openssh jq curl grep && rm -rf /var/cache/apk/*

RUN mkdir /opt/git-semver
ENV PATH="/opt/git-semver:${PATH}"

COPY src/* /opt/git-semver/
RUN chmod a+x /opt/git-semver/*

ENTRYPOINT ["/bin/bash", "-c"]
